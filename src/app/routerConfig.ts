import { Routes } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { IPComponent } from './components/ip/ip.component';

export const appRoutes: Routes = [
  {
    path: 'user',
    component: UserComponent
  },
  { path: 'ip',
    component: IPComponent
  }
];
