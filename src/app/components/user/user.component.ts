import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user/user.service";
import * as _ from 'underscore';
import {User} from "../../model/User";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {

  users: any;

  roles = ['NA', 'ADMIN', 'STANDARD'];
  formUser = new User(undefined, undefined, undefined, undefined, undefined);

  submitted = false;

  onSubmit() {
    this.submitted = true;
  }

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.get()
      .subscribe(data => {
        this.users = data;
      });
  }

  private deleteUser(user) {
    this.userService.delete(user)
      .subscribe(
        data => {
          this.users = _.reject(this.users, function(u){
            return u.id === user.id;
          });
       },
      error => {
        console.error(error);
      });
  }

  private updateName($event: any, user: User): void {
    user.name = $event.value;
    this.userService.update(user)
        .subscribe(
          data => {
           console.log('User updated succesfully.')
          },
          error => {
            console.error(error);
          });
  }

  private updateEmail($event: any, user: User): void {
    user.email = $event.value;
    this.userService.update(user)
      .subscribe(
        data => {
          console.log('User updated succesfully.')
        },
        error => {
          console.error(error);
        });
  }

  private createUser(user: User): void {

    this.userService.create(user)
      .subscribe(
        data => {
          console.log('User created successfully.');
          this.users.push(data);
        },
        error => {
          console.error(error);
        });
  }
}
