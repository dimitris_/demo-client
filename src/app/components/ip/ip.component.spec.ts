import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IPComponent } from './ip.component';

describe('IPComponent', () => {
  let component: IPComponent;
  let fixture: ComponentFixture<IPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
