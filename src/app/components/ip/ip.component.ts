import { Component, OnInit } from '@angular/core';
import {IPService} from "../../services/ip/ip.service";
import {IPLookup} from "../../model/IPLookup";
import * as _ from 'underscore';


@Component({
  selector: 'app-ip',
  templateUrl: './ip.component.html',
  styleUrls: ['./ip.component.css']
})
export class IPComponent implements OnInit {

  ipLookups: any;
  formIP = new IPLookup(undefined, undefined, undefined);

  submitted = false;

  onSubmit() {
    this.submitted = true;
  }
  constructor(private ipService: IPService) { }

  ngOnInit() {
    this.ipService.get()
      .subscribe(data => {
        this.ipLookups = data;
      });
  }

  private updateIP($event: any, ip: IPLookup): void {
    ip.ip = $event.value;
    this.ipService.update(ip)
      .subscribe(
        data => {
          console.log('IP updated succesfully.')
        },
        error => {
          console.error(error);
        });
  }

  private updateHostname($event: any, ip: IPLookup): void {
    ip.hostname = $event.value;
    this.ipService.update(ip)
      .subscribe(
        data => {
          console.log('IP updated succesfully.')
        },
        error => {
          console.error(error);
        });
  }

  private createIP(ip: IPLookup): void {

    this.ipService.create(ip)
      .subscribe(
        data => {
          console.log('IP created successfully.');
          this.ipLookups.push(data);
        },
        error => {
          console.error(error);
        });
  }

  private deleteIP(ip) {
    this.ipService.delete(ip)
      .subscribe(
        data => {
          this.ipLookups = _.reject(this.ipLookups, function(u){
            return u.id === ip.id;
          });
        },
        error => {
          console.error(error);
        });
  }

}
