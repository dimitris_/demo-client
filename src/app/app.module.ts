import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';

import { ClarityModule } from "@clr/angular";
import { UserComponent } from './components/user/user.component';
import { IPComponent } from './components/ip/ip.component';
import { RouterModule } from '@angular/router';

import { appRoutes } from './routerConfig';

import { IPService } from './services/ip/ip.service'
import { UserService } from './services/user/user.service'
import {HttpClientModule} from '@angular/common/http';

import {InputEditorModule} from 'angular-inline-editors';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxClickToEditModule } from 'ngx-click-to-edit';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule }   from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    IPComponent
  ],
  imports: [
    BrowserModule,
    ClarityModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    InputEditorModule.forRoot(),
    NgxClickToEditModule.forRoot(),
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [IPService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
