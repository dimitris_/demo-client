export class IPLookup {

  constructor(
    public id: number,
    public ip: string,
    public hostname: string
  ) {  }

}
