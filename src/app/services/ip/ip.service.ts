import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class IPService {

  result: any;
  constructor(private http: HttpClient) {}

  create(ip) {
    const uri = 'http://localhost:8080/ip/create';
    return this.http.post(uri, ip, httpOptions);
  }

  get() {
    const uri = 'http://localhost:8080/ip/fetch';
    return this.http.get(uri);
  }

  update(payload) {
    const uri = 'http://localhost:8080/ip/update';
    return this.http.post(uri, payload, httpOptions);
  }

  delete(payload) {
    const uri = 'http://localhost:8080/ip/delete';
    return this.http.post(uri, payload, httpOptions);

  }
}
