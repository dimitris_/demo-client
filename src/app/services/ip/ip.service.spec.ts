import { TestBed, inject } from '@angular/core/testing';

import { IPService } from './ip.service';

describe('IpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IPService]
    });
  });

  it('should be created', inject([IPService], (service: IPService) => {
    expect(service).toBeTruthy();
  }));
});
