import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  get() {
    const uri = 'http://localhost:8080/user/fetch';
    return this.http.get(uri);

  }

  delete(payload) {
    const uri = 'http://localhost:8080/user/delete';
    return this.http.post(uri, payload, httpOptions);
  }

  update(payload) {
    const uri = 'http://localhost:8080/user/update';
    return this.http.post(uri, payload, httpOptions);
  }

  create(payload) {
    const uri = 'http://localhost:8080/user/create';
    return this.http.post(uri, payload, httpOptions);
  }
}
